from flask import Flask, jsonify
import feedparser

app = Flask(__name__)

def parse_rss_feed(url):
    feed = feedparser.parse(url)
    return feed

@app.route('/rss_feed/<url>', methods=['GET'])
def get_rss_feed(url):
    feed = parse_rss_feed(url)
    return jsonify(feed)

if __name__ == '__main__':
    app.run(debug=True)
